import socket
import os
import subprocess

class Pinger:
    def get_ip_address(self,url):
        result={}
        flag=True
        try:
            if(not self.validate_url(url)):
                result["exit-code"]=2
                result["error"]="Invalid url"
                flag=False
        except:
            result["exit-code"]=3
            result["error"]="Error validating the url...."
            flag=False
        
        if(flag):
            try:
                ip=socket.gethostbyname(url)
                result["exit-code"]=0
                result["ip-address"]=ip
                if(self.ping_site(ip)):
                    result["status"]="Alive"
                else:
                    result["status"]="Dead"
            except:
                result["exit-code"]=1
                result["error"]="Hostname resolution failed / Invalid url"
        return result
    
    def ping_site(self,ip):
        ping_command="ping -s 64 -c 1 "+ip
        try:
            result=subprocess.Popen(ping_command,stdout=subprocess.PIPE,stderr=subprocess.PIPE,shell=True)
            (data,error)=result.communicate()
            if(result.returncode==0):
                return True
            else:
                return False
        except:
            return False
    
    def validate_url(self,url):
        url.strip()
        list=url.split(" ")
        if(len(list)>1):
            return False
        else:
            return True