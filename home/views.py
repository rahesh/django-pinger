from django.shortcuts import render
from django.http import HttpResponse
from django.template import Context
from django.template.loader import  get_template 
from django.template import RequestContext, Template

from pinger import Pinger
# Create your views here.

def index(request):
	 return render(request, 'home/get_url.html', {})

def ping(request):
    if request.method == "GET" :
        return index(request)
    elif request.method == "POST" :
        p = Pinger()
        result=p.get_ip_address(request.POST["url"])
        if result["exit-code"]==0:
            return render(request, 'home/ip.html', {'ip_address':result["ip-address"],'status':result["status"]})
        else:
            return render(request, 'home/error.html', {'error':result["error"]})
        
        
