from django.conf.urls import url,patterns
from home import views

urlpatterns=patterns('',
	url(r'^$',views.index,name="home"),
    url(r'^ping$',views.ping,name="ping"),
)
